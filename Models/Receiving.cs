﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS_FE.Models
{
    public class Receiving
    {
        public string ID { get; set; }
        public string PurchaseRequestID { get; set; }
        public string RefNumber { get; set; }
        public string SourceType { get; set; }
        public string SourceID { get; set; }
        public string SourceCode { get; set; }
        public string SourceName { get; set; }
        public string SourceAddress { get; set; }
        public string WarehouseID { get; set; }
        public string WarehouseName { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseType { get; set; }
        public string RawMaterialID { get; set; }
        public string RawMaterialCode { get; set; }
        public string RawMaterialName { get; set; }
        public string RawMaterialMaker { get; set; }
        public string Barcode { get; set; }
        public string DONo { get; set; }
        public string LotNo { get; set; }
        public bool COA { get; set; }
        public string InDate { get; set; }
        public string ExpDate { get; set; }
        public string QtyPerBag { get; set; }
        public string BagQty { get; set; }
        public string ActualBagQty { get; set; }
        public string Qty { get; set; }
        public string ActualQty { get; set; }
        public string OKQty { get; set; }
        public string OKBagQty { get; set; }
        public string NGQty { get; set; }
        public string NGBagQty { get; set; }
        public string ReturnQty { get; set; }
        public string PRQty { get; set; }
        public string PRUoM { get; set; }
        public string PRQtyPerBag { get; set; }
        public string PRBagQty { get; set; }
        public string AvailableQty { get; set; }
        public string AvailableBagQty { get; set; }
        public string RequestUoM { get; set; }
        public string UoM { get; set; }
        public string DeviationQty { get; set; }
        public string ETA { get; set; }
        public string ATA { get; set; }
        public string ReceivedBy { get; set; }
        public string ReceivedOn { get; set; }
        public string InspectionQty { get; set; }
        public string InspectionBagQty { get; set; }
        public string InspectionMethod { get; set; }
        public string InspectedBy { get; set; }
        public string InspectedOn { get; set; }
        public string NGBinRackID { get; set; }
        public string NGBinRackCode { get; set; }
        public string NGBinRackName { get; set; }
        public string TransactionStatus { get; set; }
        public string JudgementQty { get; set; }
        public string JudgementBagQty { get; set; }
        public string JudgementMethod { get; set; }
        public string JudgeBy { get; set; }
        public string JudgeOn { get; set; }
        public string BinRackID { get; set; }
        public string BinRackCode { get; set; }
        public string BinRackName { get; set; }
        public string PutawayQty { get; set; }
        public string PutawayBagQty { get; set; }
        public string PutawayMethod { get; set; }
        public string PutBy { get; set; }
        public string PutOn { get; set; }
    }
}