﻿using iText.Html2pdf;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WMS_FE.Models;

namespace WMS_FE.Controllers
{
    public class PurchaseRequestController : Controller
    {
        // GET: ProductionPlan
        public ActionResult Index()
        {
            if (Session["token"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            ViewBag.BaseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
            ViewBag.Server = ConfigurationManager.AppSettings["server"].ToString();
            return View();
        }

        public ActionResult Create()
        {
            if (Session["token"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            ViewBag.BaseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
            ViewBag.Server = ConfigurationManager.AppSettings["server"].ToString();
            return View();
        }

        public ActionResult Edit()
        {
            if (Session["token"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            ViewBag.BaseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
            ViewBag.Param = this.Request.QueryString["id"];
            ViewBag.Server = ConfigurationManager.AppSettings["server"].ToString();
            return View();
        }

        // GET: PDF
        public async Task<ActionResult> PDF(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new Exception();
                }
                else
                {
                    //get data api
                    string Domain = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
                    string ApiAddress = ConfigurationManager.AppSettings["server"].ToString();


                    HttpClient client = new HttpClient();
                    Uri uri = new Uri(ApiAddress + string.Format("Api/PurchaseRequest/ExcelDataById?id={0}", id));
                    var response = await client.GetAsync(uri);
                    string result = response.Content.ReadAsStringAsync().Result;
                    PurchaseRequestResponse res = JsonConvert.DeserializeObject<PurchaseRequestResponse>(result);

                    PurchaseRequestHeaderDTO purchaseRequest = res.data;

                    string viewName = "";
                    string title = "";

                    switch (res.data.SourceType)
                    {
                        case "VENDOR":
                            viewName = "PR_Vendor";
                            title = "PURCHASING REQUEST";
                            break;
                        case "IMPORT":
                            viewName = "PR_Vendor";
                            title = "PURCHASING REQUEST";
                            break;
                        case "CUSTOMER":
                            viewName = "PR_Customer";
                            title = "RM CUSTOMER SUPPLIED";
                            break;
                        case "OUTSOURCE":
                            viewName = "PR_Outsource";
                            title = "DELIVERY NOTE";
                            break;
                    };

                    ViewBag.Title = title;
                    ViewBag.Logo = Domain + "/Content/img/logo.jpg";
                    String body = Helper.RenderViewToString(this.ControllerContext, viewName, purchaseRequest);
                    using (MemoryStream stream = new System.IO.MemoryStream())
                    {
                        using (var pdfWriter = new PdfWriter(stream))
                        {
                            PdfDocument pdf = new PdfDocument(pdfWriter);
                            Document document = new Document(pdf);
                            if (!res.data.SourceType.Equals("OUTSOURCE"))
                            {
                                document = new Document(pdf, PageSize.A4.Rotate());
                            }


                            //int numberOfPages = pdf.GetNumberOfPages();
                            //for (int i = 1; i <= numberOfPages; i++)
                            //{

                            //    // Write aligned text to the specified by parameters point
                            //    document.ShowTextAligned(new Paragraph(String.Format("page %s of %s", i, numberOfPages)),
                            //            559, 806, i, TextAlignment.RIGHT, VerticalAlignment.BOTTOM, 0);
                            //}

                            HtmlConverter.ConvertToPdf(body, pdf, null);
                            byte[] file = stream.ToArray();
                            MemoryStream output = new MemoryStream();
                            output.Write(file, 0, file.Length);
                            output.Position = 0;

                            Response.AddHeader("content-disposition", "inline; filename=form.pdf");
                            // Return the output stream
                            return File(output, "application/pdf");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Content(@"<body>
                       <script type='text/javascript'>
                         window.close();
                       </script>
                     </body> ");
            }
        }


    }
}